## Project link
https://evo-tic-tac.herokuapp.com/

## Installation
1. virtualenv .env;
2. source .env/bin/activate;
3. (.env) pip install -r requirements.txt;
4. (.env) fab run (to start main app);