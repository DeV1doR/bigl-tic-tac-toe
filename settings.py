import os

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

DEBUG = True
SECRET_KEY = os.environ.get('SECRET_KEY',
                            '51f52814-0071-11e6-a247-000ec6c2372c')
SQLALCHEMY_DATABASE_URI = os.environ.get(
    'DATABASE_URL', 'sqlite:///' + os.path.join(BASE_DIR, 'db.sqlite'))
SQLALCHEMY_TRACK_MODIFICATIONS = True
