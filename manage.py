import subprocess

from flask_script import Manager, Shell

from tic_tac_toe import db, socketio
from runserver import app


manager = Manager(app)


@manager.command
def createdb():
    """Creates the database."""
    db.drop_all()
    db.create_all()


@manager.command
def runserver(host='127.0.0.1', port=8000):
    subprocess.call('fuser -k {0}/tcp'.format(port), shell=True)
    subprocess.call('find . -name "*.pyc" -exec rm -rf {} \;', shell=True)
    try:
        socketio.run(manager.app,
                     host=host,
                     port=port,
                     use_reloader=True)
    except KeyboardInterrupt:
        subprocess.call('fuser -k {0}/tcp'.format(port), shell=True)


def make_shell_context():
    return dict(app=app)

manager.add_command("shell", Shell(make_context=make_shell_context))


if __name__ == '__main__':
    manager.run()
