import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_socketio import SocketIO

from .tic_tac_toe import main


# Flask extensions
db = SQLAlchemy()
socketio = SocketIO()

# Import models so that they are registered with SQLAlchemy
import models  # noqa

# Import Socket.IO events so that they are registered with Flask-SocketIO
import events  # noqa


def create_app(config_name=None, async_mode='gevent'):
    if async_mode == 'gevent' or not async_mode:
        from gevent import monkey
        monkey.patch_all()
        import sqlalchemy_gevent
        sqlalchemy_gevent.patch_all()

    if config_name is None:
        config_name = os.environ.get('TTT_SETTINGS', 'settings')

    app = Flask(__name__)
    app.config.from_object(config_name)

    # install bp
    app.register_blueprint(main)

    # install flask extensions
    db.init_app(app)
    socketio.init_app(app, async_mode='gevent')

    return app
