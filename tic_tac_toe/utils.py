# -*- coding: utf-8 -*-
import time

from flask import session, g, abort
from . import db


def timestamp():
    """Return the current timestamp as an integer."""
    return int(time.time())


def get_or_create(model, **kwargs):
    instance = db.session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance, False
    else:
        instance = model(**kwargs)
        db.session.add(instance)
        db.session.commit()
        return instance, True


def simple_login_required(f):

    def wrapper(*args, **kwargs):
        from .models import ChatUser
        if session.get('username'):
            user = ChatUser.query \
                .filter_by(username=session['username']).first()
            g.current_user = user
            return f(*args, **kwargs)
        abort(400)
    return wrapper
