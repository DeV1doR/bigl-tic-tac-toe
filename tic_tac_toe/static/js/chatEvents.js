$(function() {

    $chatShowHide.on('click', function(e) {
        e.preventDefault();
        var speedTime = 'slow';
        $chatPanelBody.slideToggle(speedTime);
        $chatPanelFooter.slideToggle(speedTime);
        scrollMsgToBottom();
    });

    $chatBtn.on('click', function(e) {
        e.preventDefault();
        var msg = $chatInput.val();
        // add error response
        if (!msg) return;
        socket.emit('post:send', {"msg": msg});
        $chatInput.val('');
    });

    $chatLogin.on('click', function(e) {
        e.preventDefault();
        var msg = $chatAddUsername.val();
        if (!msg) return;
        socket.emit('user:login', {username: msg});
        $chatAddUsername.val('');
    });

    $("#btn-username").keypress(function(e) {
        if (e.which == 13) {
            $chatLogin.click();
        }
    });

    $chatLogout.on('click', function(e) {
        e.preventDefault();
        socket.emit('user:logout');
    });

    $("#btn-input").keypress(function(e) {
        if (e.which == 13) {
            $chatBtn.click();
        }
    });

});