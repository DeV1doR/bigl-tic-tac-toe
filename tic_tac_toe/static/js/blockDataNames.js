$setupMenu = $("#setup-menu");
$waitMenu = $("#wait-menu");
$gameMenu = $("#game-menu");
$finishMenu = $("#finish-menu");

$ticTable = $("#tic-table");
$usersCount = $("#user-count");

// chat
$chatPanelBody = $('.chat-panel-body');
$chatPanelFooter = $('.chat-panel-footer');
$chatShowHide = $('#minim_chat_window').parent();
$chatBtn = $("#btn-chat");
$chatInput = $("#btn-input");
$chatMsgBox = $(".msg_container_base");
$chatIcons = $(".chat-icons");

// chat login
$chatLogin = $('#btn-login');
$chatLogout = $('.chat-logout');
$chatLoggedIn = $('.user-logged-in');
$chatLoggedOut = $('.user-logged-out');
$chatShowUsername = $('#chat-username');
$chatAddUsername = $('#btn-username');