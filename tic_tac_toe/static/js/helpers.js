function range(start, count) {
    return Array.apply(0, Array(count))
        .map(function (element, index) {
            return index + start;
    });
}
function contains(needle) {
    // Per spec, the way to identify NaN is that it is not equal to itself
    var findNaN = needle !== needle;
    var indexOf;

    if(!findNaN && typeof Array.prototype.indexOf === 'function') {
        indexOf = Array.prototype.indexOf;
    } else {
        indexOf = function(needle) {
            var i = -1, index = -1;

            for(i = 0; i < this.length; i++) {
                var item = this[i];

                if((findNaN && item !== item) || item === needle) {
                    index = i;
                    break;
                }
            }

            return index;
        };
    }

    return indexOf.call(this, needle) > -1;
}
function isArray(arr) {
    return arr instanceof Array;
}
function scrollMsgToBottom() {
    var height = 1000;
    $chatMsgBox.find('.msg_container').each(function(i, value){
        height += parseInt($(this).height(), 10);
    });
    $chatMsgBox.animate({scrollTop: height});
}