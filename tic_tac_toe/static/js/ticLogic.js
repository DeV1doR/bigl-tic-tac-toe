TicHtmlSwitcher = {
    addSetupMenu: function() {
        $setupMenu.show();
        $("input[name='size']:radio:checked").trigger('change');
    },
    addWaitMenu: function() {
        $waitMenu.show();
    },
    addGameMenu: function() {
        $gameMenu.show();
    },
    addFinishMenu: function() {
        $finishMenu.show();
    },
    clearSetupMenu: function() {
        $setupMenu.hide();
    },
    clearWaitMenu: function() {
        $waitMenu.hide();
    },
    clearGameMenu: function() {
        $gameMenu.hide();
    },
    clearFinishMenu: function() {
        $finishMenu.hide();
    },
    clearAll: function() {
        this.clearFinishMenu();
        this.clearGameMenu();
        this.clearSetupMenu();
        this.clearWaitMenu();
    }

};


function TicGameApp() {

    var self = this;

    this.table = $ticTable;
    this._tableName = "#" + this.table.attr('id');
    this._repeatanceSpeed = 500;
    this._repeatanceDelay = 500;

    this.gameStatuses = {
        setup: "Setup game configs",
        wait: "Waiting opponent",
        started: "Game started",
        finished: "Game finished"
    };

    this.configs = {
        diagForb: false,
        posibleChars: ["X", "O"],
        playerFig: "X",
        shortGame: true
    };

    this.init = function() {
        this._resetDefault();
        this.clearTimers();
        this.getMenuByStatus();
    };
    this.getUserSettings = function() {
        return {
            gameStatus: this.gameStatus,
            moveCounter: this.moveCounter,
            winner: this.winner,
            tableSize: this.tableSize,
            savedSteps: this.savedSteps,
            tableCells: this.tableCells,
            _timers: this._timers
        };
    };
    this._resetDefault = function() {
        this.gameStatus = this.gameStatuses.init;
        this.moveCounter = 0;
        this.winner = null;
        this.tableSize = null;
        this.savedSteps = {};
        this.tableCells = [];
        this._timers = [];
    };
    this.getMenuByStatus = function() {
        TicHtmlSwitcher.clearAll();
        if (this.gameStatus == this.gameStatuses.wait) {
            TicHtmlSwitcher.addWaitMenu();
        } else if (this.gameStatus == this.gameStatuses.started) {
            TicHtmlSwitcher.addGameMenu();
        } else if (this.gameStatus == this.gameStatuses.finished) {
            TicHtmlSwitcher.addFinishMenu();
        } else {
            TicHtmlSwitcher.addSetupMenu();
        }
    };
    this.updateGameSettings = function(settings) {
        for (var key in settings) {
            this[key] = settings[key];
        }
    };
    this.updateGameBoard = function(settings) {
        this.updateGameSettings(settings);
        this.getMenuByStatus();
        this.drawLastTable();
    };
    this.toStart = function() {
        TicHtmlSwitcher.clearSetupMenu();
        TicHtmlSwitcher.addGameMenu();
        this.gameStatus = TIC_GAME.gameStatuses.started;
    };
    this.toYield = function() {
        TicHtmlSwitcher.clearGameMenu();
        TicHtmlSwitcher.addFinishMenu();
        this.gameStatus = TIC_GAME.gameStatuses.finished;
    };
    this.toRepeat = function() {
        this.clearHtmlTable();
        this.clearTimers();
        this.repeatSteps();
    };
    this._getWinCombinations = function(posArr) {
        var x = posArr[0], y = posArr[1];
        return {
            1: [posArr, [x + 1, y], [x + 2, y]],
            2: [posArr, [x, y + 1], [x, y + 2]],
            3: [posArr, [x + 1, y + 1], [x + 2, y + 2]],
            4: [posArr, [x - 1, y], [x - 2, y]],

            5: [posArr, [x - 1, y + 1], [x - 2, y + 2]],
            6: [posArr, [x, y - 1], [x, y - 2]],
            7: [posArr, [x - 1, y - 1], [x - 2, y - 2]],
            8: [posArr, [x + 1, y - 1], [x + 2, y - 2]],

            9: [[x - 1, y], posArr, [x + 1, y]],
            10: [[x, y - 1], posArr, [x, y + 1]],
            11: [[x - 1, y - 1], posArr, [x + 1, y + 1]],
            12: [[x + 1, y - 1], posArr, [x - 1, y + 1]],
        };
    };
    this.__isWinArrIsTrue = function(array) {
        if (array.length != 2) return;
        for (var i = 0; i < array.length; i++) {
            if (array[i] === false) return false;
        }
        return true;
    };
    this.getWinsComb = function(cell) {
        var posArr = this.getCellCoord(cell),
            combinations = this._getWinCombinations(posArr),
            winKey = false, winArr, possCell, winStrick = false;
        $.each(combinations, function(keyComb, combination) {
            winArr = [];
            $.each(combination, function(i, posStep) {
                if (posStep[0] >= 0 && posStep[1] >= 0 && posStep !== posArr &&
                    this.tableSize != posStep[0] && this.tableSize != posStep[1]) {

                    possCell = self._getCellSelector(posStep);

                    if (possCell.html() == self.configs.playerFig) {
                        winArr.push(true);
                    } else {
                        winArr.push(false);
                    }
                }
            });
            if (self.__isWinArrIsTrue(winArr)) {
                winStrick = combinations[keyComb];
            }
        });
        return winStrick;
    };
    this.checkWinCondition = function(cell) {
        if (this.getWinsComb(cell)) {
            this.winner = this.configs.playerFig;
        }

    };
    this.resetRows = function(counter) {
        this._resetDefault();
        for (var x = 0; x < counter; x++) {
            this.tableCells[x] = [];
            for (var c = 0; c < counter; c++) {
                this.tableCells[x][c] = 0;
            }
        }
    };
    this.clearHtmlTable = function() {
        $.each(this.table.find('tr'), function(i, row) {
            $.each($(this).find('td'), function(j, col) {
                $(this).html('');
            });
        });
    };
    this.clearTimers = function() {
        $.each(this._timers, function(i, timer) { clearTimeout(timer); });
        this._timers = [];
    };
    this.rescanTable = function() {
        // rescan table for saving last positions of x-o
        $.each(this.table.find('tr'), function(i, row) {
            self.tableCells[i] = [];
            $.each($(this).find('td'), function(j, col) {
                var value = $(this).html();
                if (!self.configs.posibleChars.includes(value)) {
                    value = 0;
                }
                self.tableCells[i][j] = value;
            });
        });
    };
    this.drawLastTable = function() {
        $.each(this.tableCells, function(i, row) {
            $.each(row, function(j, fig) {
                if (!fig) {
                    fig = "";
                }
                self.cellDraw([i, j], fig);
            });
        });
    };
    this.saveCurrentPosStep = function(tdObj) {
        this.savedSteps[this.moveCounter] = {
            "fig": this.configs.playerFig,
            "pos": this.getCellCoord(tdObj)
        };
        this.moveCounter++;
    };
    this.getCellCoord = function(tdObj) {
        return [tdObj.parentNode.rowIndex, tdObj.cellIndex];
    };
    this.repeatSteps = function() {
        // repeat saved steps
        if (this.gameStatus != this.gameStatuses.finished) {
            return false;
        }
        savedSpeed = this._repeatanceSpeed;
        $.each(this.savedSteps, function(key, step) {
            self._asyncCellDraw(step);
        });
        this._repeatanceSpeed = savedSpeed;
    };
    this._asyncCellDraw = function(step) {
        var timer = setTimeout(function() {
            self.cellDraw(step.pos, step.fig);
        }, self._repeatanceSpeed);
        this._timers.push(timer);
        self._repeatanceSpeed += self._repeatanceDelay;
    };
    this.cellDraw = function(posArr, fig) {
        var cell = this._getCellSelector(posArr);
        if (typeof fig === "undefined") {
            fig = this.configs.playerFig;
        }
        cell.html(fig);
    };
    this.cellDrawWithSave = function(cell) {
        // to get from native dom element
        var cellCoords = this.getCellCoord(cell);
        if (this.gameStatus != this.gameStatuses.started) {
            return false;
        }
        if (this.configs.diagForb && this.cellOnDiag(cellCoords)) {
            return false;
        }
        this.saveCurrentPosStep(cell);
        this.checkWinCondition(cell);
        this.cellDraw(cellCoords);
    };
    this._getCellSelector = function(posArr) {
        return $(this._tableName)
            .find('tr:eq(' + posArr[0] + ') > td:eq(' + posArr[1] + ')');
    };
    this.cellOnDiag = function(posArr) {
        var isOnCell = false,
            diagCellsCoords = [
                [posArr[0] - 1, posArr[1] - 1],
                [posArr[0] - 1, posArr[1] + 1],
                [posArr[0] + 1, posArr[1] - 1],
                [posArr[0] + 1, posArr[1] + 1]
            ];
        $.each(diagCellsCoords, function(i, cellCoords) {
            if (cellCoords[0] >= 0 && cellCoords[1] >= 0) {
                var cell = self._getCellSelector(cellCoords);
                if (cell.html() == self.configs.playerFig) {
                    isOnCell = true;
                }
            }
        });
        return isOnCell;
    };
    this.changeTableSize = function(radioSelector) {
        var counter = $(radioSelector).attr('id').match(/\d+/g),
            boxBlock = '<td class="tic-box"></td>',
            boxBlocks = "", rowBlocks = "",
            rowBlock = function row_block(boxBlocks) {
                return '<tr>' + boxBlocks + '</tr>';
            };
        for (var i = 0; i < counter; i++) {
            boxBlocks += boxBlock;
        }
        for (var j = 0; j < counter; j++) {
            rowBlocks += rowBlock(boxBlocks);
        }
        this.table.html(rowBlocks);
        this.resetRows(counter);
        this.tableSize = parseInt(counter, 10);
    };
}
