var socket = io();



socket.on('connect', function() {

    window.TIC_GAME = new TicGameApp();
    TIC_GAME.init();
    console.log('Client socket opened');

    socket.on('disconnect', function() {
        socket.emit('disconnect');
    });

    socket.on('message', function(data) {
        console.log(data);
    });

    // socket.on('clients:manage', function(clients) {
    //     $("#user-count").html(clients.length);
    //     console.log(clients);
    // });

    socket.on('post:receive', function(data) {
        $chatMsgBox.append(data.model.html);
        scrollMsgToBottom();
    });

    socket.on('messages:show', function(msgsList) {
        $.each(msgsList, function(i, msg) {
            $chatMsgBox.append(msg);
        });
    });

    socket.on('user:logged-in', function(data) {
        console.log("User logged in");
        console.log(data);
        $chatLoggedOut.hide();
        $chatLoggedIn.show();
        $chatShowUsername.html(" " + data.model.username);
    });

    socket.on('user:logged-out', function() {
        console.log("User logged out");
        $chatLoggedOut.show();
        $chatLoggedIn.hide();
        $chatShowUsername.empty();
    });
});

// not disconnect fired in gevent
/*$(window).on('beforeunload',function() {
    socket.disconnect();
});*/
