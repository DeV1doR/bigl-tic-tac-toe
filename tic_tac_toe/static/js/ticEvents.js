$(function() {

    $(document).on('change', "input[name='size']:radio", function(e) {
        TIC_GAME.changeTableSize(this);
    });

    $(document).on("click", "#tic-table td", function(e) {
        if (!$(this).html()) {
            TIC_GAME.cellDrawWithSave(this);
            TIC_GAME.rescanTable();
            if (TIC_GAME.winner) {
                $('#tic-game-yield').click();
            }
        }
    });

    $(document).on('click', '#tic-game-repeat', function(e) {
        TIC_GAME.toRepeat();
    });

    $(document).on('change', '#tic-game-fig', function(e) {
        TIC_GAME.configs.playerFig = $(this).find(':selected').html();
    });

    $(document).on('click', '#tic-game-start', function(e) {
        TIC_GAME.toStart();
        // gameSocket.emit('update game board', TIC_GAME.getUserSettings());
    });

    $(document).on('click', '#tic-game-yield', function(e) {
        TIC_GAME.toYield();
        // gameSocket.emit('update game board', TIC_GAME.getUserSettings());
    });

    $(document).on('click', '#tic-game-new-game', function(e) {
        TIC_GAME.init();
    });

    $(document).on('click', '#short-game', function(e) {
        TIC_GAME.configs.shortGame = $(this).is(':checked');
    });

    $(document).on('click', '#diag-forb', function(e) {
        TIC_GAME.configs.diagForb = $(this).is(':checked');
    });

});