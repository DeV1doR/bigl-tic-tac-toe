from flask import Blueprint, render_template, session, g


main = Blueprint('main', __name__)


@main.route('/')
def main_page():
    return render_template('index.html')


@main.before_request
def update_session_user():
    from .models import ChatUser
    if session.get('username'):
        user = ChatUser.query \
            .filter_by(username=session['username']).first()
        g.current_user = user
