# -*- coding: utf-8 -*-
from flask import g, request, session

from . import socketio, db
from .models import ChatUser, Message
from .utils import get_or_create, simple_login_required


# clients = []


def push_model(event, model, with_broadcast=True):
    """Push the model to all connected Socket.IO clients."""
    socketio.emit(event, {'class': model.__class__.__name__.lower(),
                  'model': model.to_dict()}, broadcast=with_broadcast)


@socketio.on('connect')
def ws_connect():
    # clients.append(request.remote_addr)
    # socketio.emit('clients:manage', clients, broadcast=True)
    socketio.emit('messages:show', [msg.html for msg in Message.query.all()])
    socketio.send('Server connected')


@socketio.on('disconnect')
def ws_disconnect():
    # clients.remove(request.remote_addr)
    # socketio.emit('clients:manage', clients, broadcast=True)
    socketio.send('Server disconnected')


@socketio.on('user:login')
def ws_user_login(data):
    if session.get('username'):
        return True
    user, _ = get_or_create(ChatUser, **data)

    user.ping()
    db.session.commit()

    g.current_user = user
    session['username'] = user.username
    push_model('user:logged-in', user, False)

    # create message that user joined chat
    msg = Message.create_user_status_message(user, 'connected')
    push_model('post:receive', msg)

    # close db session
    db.session.remove()


@socketio.on('user:logout')
@simple_login_required
def ws_user_logout():
    username = session.get('username')
    if username:
        user = ChatUser.query.filter_by(username=username).first()
        if user:
            user.online = False
            db.session.commit()

            del session['username']

            # create message that user disconnected from chat
            msg = Message.create_user_status_message(user, 'disconnected')
            push_model('post:receive', msg)
            socketio.emit('user:logged-out')
            db.session.remove()


@socketio.on('post:send')
@simple_login_required
def ws_post_send(data):
    # create message for chat
    msg = Message.create_post_message(g.current_user, data['msg'])
    push_model('post:receive', msg)

    db.session.remove()
