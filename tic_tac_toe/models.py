# -*- coding: utf-8 -*-
import cgi
import ast
from datetime import datetime

from flask import g, get_template_attribute

from . import db
from .utils import timestamp


class ChatUser(db.Model):
    """Cutted class of User model.
    Used only username and password, created by username.
    """

    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(25), nullable=False, unique=True)
    online = db.Column(db.Boolean, default=False)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    messages = db.relationship('Message', backref='user')
    last_seen_at = db.Column(db.Integer, default=timestamp)

    def __init__(self, username):
        self.username = username

    def __repr__(self):
        return '<User %r>' % (self.username)

    def ping(self):
        """Marks the user as recently seen and online."""
        self.last_seen_at = timestamp()
        last_online = self.online
        self.online = True
        return last_online != self.online

    @property
    def chat_role(self):
        if g.current_user == self:
            return 'sent'
        return 'receive'

    @classmethod
    def create(cls, username):
        """Create a new user."""
        user = cls(username=username)
        db.session.add(user)
        db.session.commit()
        return user

    def to_dict(self):
        """Export user to a dictionary."""
        return {
            'id': self.id,
            'username': self.username,
            'online': self.online,
            'is_active': self.is_active,
        }


class Message(db.Model):
    """The Message model.
    """

    __tablename__ = 'messages'

    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.Integer, default=timestamp)
    source = db.Column(db.Text, default='')
    html = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    def __repr__(self):
        return '<Message %r>' % (self.source[:10])

    @classmethod
    def create_post_message(cls, user, source):
        msg = cls()
        msg.source = source
        msg.user = user
        msg.create_html_message(source)
        db.session.add(msg)
        db.session.commit()
        return msg

    @classmethod
    def create_user_status_message(cls, user, status):
        msg = cls()
        msg.user_status_html_message(user, status)
        msg.user = user
        db.session.add(msg)
        db.session.commit()
        return msg

    def user_status_html_message(self, user, status='connected'):
        if status == 'connected':
            _status_html = 'joined chat'
        else:
            _status_html = 'disconnected from chat'
        self.html = '<div class="row msg_container">{0} {1}</div>' \
            .format(user.username, _status_html)

    def create_html_message(self, source):
        chat_post = get_template_attribute('macro/chat_msg.html',
                                           'chat_post')
        self.html = chat_post(self.user, cgi.escape(source), datetime.now())

    def to_dict(self):
        """Export message to a dictionary."""
        return {
            'id': self.id,
            'created_at': self.created_at,
            'source': self.source,
            'html': self.html,
            'posted_by': self.user.username,
        }


# class GameBoard(db.Model):
#     """Game board of game.
#     """
#     GAME_STATUSES = (
#         ("pending", "Waiting opponent"),
#         ("started", "Game started"),
#         ("finished", "Game finished"),
#     )

#     __tablename__ = 'game_board'

#     id = db.Column(db.Integer, primary_key=True)
#     created_by = db.relationship("ChatUser", uselist=False)
#     created_by_id = db.Column(db.Integer, db.ForeignKey('users.id'),
#                               nullable=False)
#     accepted_by = db.relationship("ChatUser", uselist=False)
#     accepted_by_id = db.Column(db.Integer, db.ForeignKey('users.id'))
#     status = db.Column(db.Text, nullable=False, default=GAME_STATUSES[0][0])
#     created_at = db.Column(db.Integer, default=timestamp)
#     finished_at = db.Column(db.Integer, default=timestamp)
#     coordinates_str = db.Column(db.String, nullable=False)

#     def __init__(self, created_by, accepted_by, coordinates):
#         self.created_by = created_by
#         self.accepted_by = accepted_by
#         self.coordinates = coordinates

#     @property
#     def coordinates(self):
#         return ast.literal_eval(self.coordinates_str)

#     @coordinates.setter
#     def coordinates(self, coordinates):
#         if isinstance(coordinates, basestring):
#             raise AttributeError(
#                 'Coordinates must be list or tuple, got string.')
#         self.coordinates_str = str(coordinates)

#     @classmethod
#     def create(cls, created_by, accepted_by, coordinates):
#         game_board = cls(created_by=created_by, accepted_by=accepted_by,
#                          coordinates=coordinates)
#         db.session.add(game_board)
#         db.session.commit()
#         return game_board

#     def to_dict(self):
#         """Export message to a dictionary."""
#         return {
#             'id': self.id,
#             'created_by': self.created_by.username,
#             'accepted_by': self.accepted_by.username,
#             'status': self.status,
#             'created_at': self.created_at,
#             'finished_at': self.finished_at,
#             'coordinates': self.coordinates
#         }
